# Prises de notes durant le test



## Installation des composants.

 - 1 - Installer node.js 

Pour cela aller sur le site [noide.js](https://nodejs.org/en/download/)

Cela va prendre un peu de temps à tout installer - il faut être patient

 - 2 - Vérifier que node.js a bien été installé en tapant dans windows+R / cmd 
   - node.js -v


Normalement cela affiche le numéro de version

 - 3 - Installer Yarn

Utiliser le [lien direct](https://www.liquidweb.com/kb/how-to-install-yarn-on-windows/) ou encore aller sur le site de [yarn](https://classic.yarnpkg.com/en/docs/install#windows-stable)

Redémarrer l'ordinateur. Ce n'est pas forcément nécessaire, mais j'ai eu besoin de le faire.

 - 4 - Vérifier que yarn est bien installé avec la commande

    -  yarn -v

Normalement un numéro de version s'affiche.

## Fork du test et préparation de l'espace

- fork du [test](https://gitlab.com/magnum2/magnum-test-internship)
- lancement de VScode
- Lancer un nouveau terminal et dedans taper
      - yarn
      - yarn start

L'affichage des données se fera alors dans IE si il a été configuré ainsi.

## Affichage de la partie preview

Attention : le rafraichissement ne fonctionne pas toujours correctement. Essayer de passer en navigation privée.

 -  src\Builder\index.js


Dedans on peut modifier tout ce qui doit être affiché.



